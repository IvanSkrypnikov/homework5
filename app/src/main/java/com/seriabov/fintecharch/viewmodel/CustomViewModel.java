package com.seriabov.fintecharch.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.seriabov.fintecharch.dto.CoinInfo;
import com.seriabov.fintecharch.model.AppDelegate;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomViewModel extends AndroidViewModel {

    private MutableLiveData<List<CoinInfo>> data = null;
    private Throwable error = null;

    public Throwable getError() {
        return error;
    }

    public CustomViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<CoinInfo>> getData(boolean isRefresh) {
        if (data == null || isRefresh) {
            data = new MutableLiveData<>();
            loadData();
        }
        return data;
    }

    private void loadData() {
        AppDelegate.from(getApplication())
                .getApiService()
                .getCoinsList()
                .enqueue(new Callback<List<CoinInfo>>() {
                    @Override
                    public void onResponse(Call<List<CoinInfo>> call, Response<List<CoinInfo>> response) {
                        data.postValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<CoinInfo>> call, Throwable t) {
                        data = null;
                        error = t;
                    }
                });
    }
}
